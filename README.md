# Terminology and abbreviations

## Terminology and abbreviations

-   **DAW:** Digital audio workstation, type of software commonly used in music
    production.
-   **VCS:** Version control system, a system which keeps tracks of changes in
    files.
-   **LP:** Literate programming, a programming paradigm in which the program is
    written as an explanation in both text and code.

# About

## About

The elk project was started by Mathias Engel and Sakse Dalum to create a music
production tool in which everything can be done from within plain text files.

Most modern DAWs have an XML project file which describes how to setup the
project inside the software, with file links to tons of different files inside
sub&#x2013;directories. Although such a project can be managed by a VCS, conflicts
arising from XML files being modified by the software can be extremely tedious
to resolve, and the outcome is not always what is expected. This puts a limit
on the ways in which music can be made collaboratively on different computers
by different people. Although many DAWs also includes some kind of logbook
feature, allowing the music producer to write notes and communicate intent,
such features lack the finesse of source code comments or the natural language
used in LP. Moving to entirely text&#x2013;based music production overcomes these
shortcomings at the expense of a more exposed technical backbone. This approach
might not be for everyone, so if you are uncomfortable leaving your heavily
graphical user interface behind and don't need to collaborate or express your
intent with music projects, you are probably better off not spending time on
getting into elk. That being said, elk is also a very powerful tool for
creating short audio samples which can be imported into your DAW, so you can
still "harness some elk power without riding the elk".

### Project structure

elk is the epitome of an LP project: everything is kept inside org files. These
org files are then tangled to produce source files or weaved to produce
human&#x2013;readable outputs. This includes files used by the VCS such as
`.gitignore` and README.md, as well as the LICENSE and all of the python
files. The `.gitignore` is very strict, and ignores almost everything by
default, so if you want to include something that falls outside the defaults,
you need to edit it below.

### Submitting patches

Literally (no pun intended) everything is generated from org files, and patches
submitted which contain direct changes to the generated files will be
rejected. That being said, we are a friendly bunch, so if you provide us with a
high quality patch that was not written to the org files, we will help you
implement your changes properly, so that your work can be shared with others
upstream.