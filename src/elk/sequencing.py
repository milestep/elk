# This file is part of elk.
# 
# elk is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# elk is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# elk.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from typing import Mapping, Iterable

from .base import ElkBaseClass, AudioFrame

class Soundbank:
    """Soundbank(mapping, numchannels=1)"""
    def __init__(self, mapping: Mapping[str, AudioFrame],
                 numchannels=1) -> None:
        """Create a Soundbank."""

        self.mapping = mapping
        self.numchannels = numchannels

    def __repr__(self) -> str:
        return ("Soundbank(mapping={}, numchannels={})"
                .format({*self.mapping.keys()}, self.numchannels))
    def __setitem__(self, name: str, val: AudioFrame) -> None:
        if (hasattr(val, '__getitem__')):
            if (hasattr(val['__getitem__'], '__annotations__') and
                val['__getitem__']['__annotations__'].get('return') not in
                ['AudioFrame', AudioFrame]):
                raise TypeError("val.__getitem__ says that it does not "
                                "return an AudioFrame")
            self.mapping[name] = val
        else:
            raise TypeError("val must implement __getitem__")
    def __getitem__(self, name: str) -> AudioFrame:
        return self.mapping[name]
    def get(self, name: str, s: slice) -> AudioFrame:
        val = self.mapping.get(name, AudioFrame(np.array([[]])))[s]
        if isinstance(val, AudioFrame):
            if val.numchannels != self.numchannels:
                val = val.collapse().expand(self.numchannels)
            return val
        else:
            raise TypeError("soundbank.get({}, {}) returned type {} "
                            "instead of AudioFrame".format(name, s, type(val)))

class Sequencer(ElkBaseClass):
    """Sequencer(soundbank, **kwargs)"""
    def __init__(self, soundbank: Soundbank, **kwargs) -> None:
        """Create a Sequencer."""
        ElkBaseClass.__init__(self, **kwargs)

        self.soundbank = soundbank

    def __repr__(self) -> str:
        return ("Sequencer(soundbank={}, bpm={}, rate={}, sig={})"
                .format(self.soundbank, self.bpm, self.rate, self.sig))
    def __call__(self, pattern: 'Pattern', *, overlays: int = 0,
                 end: float = None) -> AudioFrame:
        if end is None:
            max_time = max(pattern.times)
            end = max_time + self.sig - max_time % self.sig
    
        frame = AudioFrame(np.zeros((self.soundbank.numchannels,
                                     int(end * self.rate * 60 / self.bpm))),
                           **self.properties)
        for item, subpat in zip(pattern.items, pattern.subpats):
            for time, step in zip(pattern.times, subpat):
                if step:
                    lsound, rsound = (
                        eval("self.soundbank.get(item, np.s_[{}])"
                             .format(step)).cut(
                                 frame[time:].length, sample_precision=True))
                    frame[time:] += lsound
    
                    for _ in range(overlays):
                        if rsound.length > 0:
                            lsound, rsound = rsound.cut(frame.length,
                                                        sample_precision=True)
                            frame[:] += lsound
        return frame

class Pattern:
    """Pattern(patlist)"""
    def __init__(self, patlist: Iterable[Iterable[str]], *,
                 times: Iterable[str] = None,
                 items: Iterable[str] = None, namespace={}) -> None:
        """Create a Pattern."""

        self.subpats = []
        self.items = []

        for line in patlist:
            if line[0] == '/t/' and not times:
                self.times = [eval(str(t), namespace) for t in line[1:]]
                continue

            if not items:
                self.items.append(line[0])

            self.subpats.append(line[1:])

        if not hasattr(self, 'times'):
            raise ValueError("missing times or patlist missing a '/t/' entry")
        self.times = times if times else self.times

        self.items = items if items else self.items
        if len(self.items) != len(self.subpats):
            raise ValueError("supplied items len does not match subpats len")

    def __repr__(self) -> str:
        return ("Pattern(times={times}, items={items}, subpats={subpats})"
                .format(**self.__dict__))
