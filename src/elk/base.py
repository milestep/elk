# This file is part of elk.
# 
# elk is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# elk is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# elk.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from typing import Union, Callable, Iterator, Mapping

global_rate = 44100
global_bpm = 120.0
global_sig = 4

def set_rate(rate: int) -> None:
    global global_rate
    global_rate = rate

def set_bpm(bpm: float) -> None:
    global global_bpm
    if bpm > 0:
        global_bpm = bpm

def set_sig(sig: float) -> None:
    global global_sig
    if sig > 0:
        global_sig = sig

def get_rate() -> int:
    return global_rate

def get_bpm() -> float:
    return global_bpm

def get_sig() -> float:
    return global_sig

def read(fname, **kwargs) -> 'AudioFrame':
    from scipy.io.wavfile import read
    rate, array = read(fname)
    return AudioFrame(array, name=fname.replace('.wav', ''),
                      **{'rate': rate, **kwargs})

class ElkBaseClass:
    def __init__(self, bpm: float = None, rate: int = None, sig: float = None):
        self.set_rate(rate)
        self.set_sig(sig)
        self.set_bpm(bpm)

    @property
    def bpm(self) -> float:
        """Beats per minute of the frame.

        This property is equal to the global beats per minute if bpm is None. Use
        instance.set_bpm(bpm) to change the beats per minute of the frame.

        """
        global get_bpm
        return get_bpm() if self._bpm is None else self._bpm

    def set_bpm(self, bpm: float) -> None:
        """Set the bpm.

        If bpm is zero, sets the bpm to the maximum value, self.rate * 60.

        """
        self._bpm = (bpm if bpm is None or 0 < bpm < self.rate * 60 else
                     self.rate * 60)

    @property
    def rate(self) -> int:
        """The samplerate of the frame.

        This property is equal to the global samplerate if rate is None. Use
        instance.set_rate(rate) to change the samplerate.

        """
        global get_rate
        return get_rate() if self._rate is None else self._rate

    def set_rate(self, rate: int) -> None:
        """Set the samplerate."""
        self._rate = rate

    @property
    def sig(self) -> float:
        """The time signature of the frame.

        This property is equal to the global signature if sig is None. Use
        instance.set_sig(sig) to change the signature.

        """
        global get_sig
        return get_sig() if self._sig is None else self._sig

    def set_sig(self, sig: float) -> None:
        """Set the samplerate."""
        self._sig = sig

    @property
    def properties(self):
        return {'bpm': self._bpm, 'rate': self._rate, 'sig': self._sig}

    def sync(self, frac):
        """Return a frequency generator computed from a fraction of the beat."""
        return iter(lambda: frac * self.bpm / 60, None)

class AudioFrame(ElkBaseClass):
    """AudioFrame(array, name='temp', **kwargs)"""
    def __init__(self, array: np.ndarray, name="temp", **kwargs) -> None:
        """Create an AudioFrame."""
        if not isinstance(array, np.ndarray) or len(array.shape) < 2:
            array = np.array(array, ndmin=2)
        if len(array.shape) != 2:
            raise TypeError("array dimensions must be exactly 2")

        ElkBaseClass.__init__(self, **kwargs)

        self.array = array
        self.name = name

    def __repr__(self):
        return ("AudioFrame({}, name={}, numchannels={}, length={}, "
                "bpm={}, rate={}, sig={})".format(
                    self.array, self.name, self.numchannels, self.length,
                    self.bpm, self.rate, self.sig))
    @property
    def numchannels(self):
        """Number of channels of the frame."""
        return self.array.shape[0]
    
    @property
    def length(self):
        """Length of the audio array."""
        return self.array.shape[1]
    
    @property
    def out_array(self):
        """Array used when writing and plotting."""
        return self.array.clip(-1, 1).swapaxes(0, 1)
    def __len__(self):
        return self.length
    def copy(self, **kwargs) -> 'AudioFrame':
        """Return a copy of self with variables substituted by **kwargs."""
        return AudioFrame(**{'array': self.array.copy(), **self.properties,
                             **kwargs})
    
    def collapse(self) -> 'AudioFrame':
        """Return a new sample by collapsing all channels into one.
    
        This method preserves the total power (or perceived loudness) of all the
        channels. The sample array is a sum over all channels, divided by the
        square root of the total number of channels. This is done in order to
        ensure that expanding and subsequently collapsing a mono channel does
        not double the signal strength.
    
        """
        return AudioFrame(np.sum(self.array, axis=0) / np.sqrt(self.numchannels),
                      **self.properties)
    
    def expand(self, n: int, expand_channel: int = 0) -> 'AudioFrame':
        """Return a new sample by expanding one channel into multiple copies.
    
        This method preserves the total power (or perceived loudness) of all the
        channels. The sample array contains n copies of the expanded channel
        divided by the square root of the total number of channel copies. This
        is done in order to ensure that expanding and subsequently collapsing a
        mono channel does not double the signal strength.
    
        """
        stacked_array = np.vstack([self.array[expand_channel,:]] * n)
        return AudioFrame(stacked_array / np.sqrt(n), **self.properties)
    def clip(self, lmin, lmax):
        """Clip (limit) the values in the frame array."""
        return self.copy(array=self.array.clip(lmin, lmax))
    def write(self, fname=None):
        from scipy.io.wavfile import write
        fname = fname if fname is not None else "{}.wav".format(self.name)
        write(fname, int(self.rate), self.out_array)
    def plot(self, fname=None):
        """Plots the frame."""
        fname = fname if fname is not None else "{}".format(self.name)
    
        import matplotlib.pyplot as plt
        plt.plot(np.linspace(0, self.length / self.rate, self.length),
             self.out_array, 'k-')
        plt.ylim(-1, 1)
        plt.xlabel('t [s]')
        plt.savefig(fname)
        plt.clf()
    def spectrum(self, fname=None, *, channel=slice(None), cutoff_db=-30):
        """Plots the frequency spectrum of the frame.
    
        If the optional kwarg channel is given (slice or int), that channel of the
        frame is plotted.
    
        The keyword argument cutoff_db (default -30dB) is the ylim bottom cutoff of
        the spectrum.
    
        """
        fname = fname if fname is not None else "{}-spec".format(self.name)
    
        import matplotlib.pyplot as plt
        from numpy.fft import fft, fftfreq
    
        powerspec = 10 * np.log10(
            abs(fft(self.out_array[:, channel], axis=0)) / self.length)
        #import pdb; pdb.set_trace()
        plt.plot(fftfreq(len(powerspec), 1 / self.rate), powerspec)
        plt.ylim(cutoff_db, 0)
        plt.xlim(1, self.rate / 2)
        plt.xlabel('f [Hz]')
        plt.ylabel('vol [dB]')
        plt.savefig(fname)
        plt.clf()
    def _key_to_arr_slice(self, key: Union[int, float, slice]):
        """Converts a slice given in beats into an array slice."""
        chan, beat = key if isinstance(key, tuple) else (slice(None), key)
        beat_to_sample = lambda beat: int(beat * self.rate * 60 // self.bpm)
        if isinstance(beat, (int, float)):
            samp = beat_to_sample(beat)
        else:
            samp = slice(*(s if s is None else beat_to_sample(s)
                           for s in (beat.start, beat.stop, beat.step)))
        return (chan, samp)
    
    def __getitem__(self, key):
        return AudioFrame(self.array[self._key_to_arr_slice(key)],
                          **self.properties)
    
    def __setitem__(self, key, val):
        if isinstance(val, AudioFrame):
            val = val.array
        self.array[self._key_to_arr_slice(key)] = val
    def __add__(self, other: 'AudioFrame') -> 'AudioFrame':
        """Return AudioFrame(self.array + other.array).
    
        Adding AudioFrames requires the two frames to have equal length and number
        of channels. When adding two frames, the new frame is instantiated with bpm
        and rate of the left--most frame.
    
        """
        if isinstance(other, AudioFrame):
            if self.numchannels != other.numchannels:
                raise TypeError("cannot mix frames with different channel "
                                "numbers: try using the AudioFrame.expand or "
                                "AudioFrame.collapse methods on either frame "
                                "before mixing")
            if self.length != other.length:
                raise TypeError("cannot mix frames of different lengths: "
                                "use slicing or fill to resize the frames before "
                                "mixing")
            return AudioFrame(self.array + other.array, **self.properties)
    
        return NotImplemented
    def cut(self, point: int, sample_precision: bool = False) -> 'AudioFrame':
        """Cut a frame at point and return the left and right remainders.
    
        If point is longer than the frame, the frame will be extended with
        zeros. In any case, the underlying array is copied before the frames are
        returned.
    
        If the optional sample_precision is True, point will be interpreted as a
        sample number instead of beat.
    
        """
        point = int(point if sample_precision else
                    point * self.rate * 60 / self.bpm)
        end = min(point, self.length)
        overflow = point - end
        overflow_array = np.zeros((self.numchannels, overflow))
        array_copy = self.array.copy()
        left_array = np.hstack((array_copy[:, :end], overflow_array))
        right_array = array_copy[:, end:]
    
        return (AudioFrame(left_array, **self.properties),
                AudioFrame(right_array, **self.properties))
    def __or__(self, other: 'AudioFrame') -> 'AudioFrame':
        """Return AudioFrame(numpy.hstack((self.array, other.array))).
    
        Concatenating AudioFrames requires the two frames to have equal number of
        channels. When concatenating two frames, the new frame is instantiated with
        bpm and rate of the left--most frame.
    
        """
        if isinstance(other, AudioFrame):
            if self.numchannels != other.numchannels:
                raise TypeError("cannot mix frames with different channel "
                                "numbers: try using the AudioFrame.expand or "
                                "AudioFrame.collapse methods on either frame "
                                "before mixing")
            return AudioFrame(np.hstack((self.array, other.array)),
                              **self.properties)
        return NotImplemented
    def __mul__(self, other: Union['AudioFrame', int]) -> 'AudioFrame':
        """Return AudioFrame(self.array * other.array).
    
        Modulating AudioFrames requires the two frames to have equal length and
        number of channels. When modulating two frames, the new frame is
        instantiated with bpm and rate of the left--most frame.
    
        If other is an integer n, return n concatenated copies of self, similar to
        self | self | ... n times.
    
        """
        if isinstance(other, AudioFrame):
            if self.numchannels != other.numchannels:
                raise TypeError("cannot modulate frames with different channel "
                                "numbers: try using the AudioFrame.expand or "
                                "AudioFrame.collapse methods on either frame "
                                "before mixing")
            if self.length != other.length:
                raise TypeError("cannot modulate frames of different lengths: "
                                "use slicing to resize the frames before mixing")
            return AudioFrame(self.array * other.array, **self.properties)
    
        elif isinstance(other, int):
            return AudioFrame(np.hstack((self.array,) * other), **self.properties)
    
        return NotImplemented
    def __pos__(self) -> 'AudioFrame':
        """Return self.expand(2)."""
        return self.expand(2)
    
    def __neg__(self) -> 'AudioFrame':
        """Return self.collapse()."""
        return self.collapse()
    def __matmul__(self, other: Union['AudioFrame', int, float]) -> 'AudioFrame':
        """Return AudioFrame(numpy.vstack((self.array, other.array))).
    
        Stacking AudioFrames requires the two frames to have equal length. When
        stacking two frames, the new frame is instantiated with bpm and rate of
        the left--most frame.
    
        If other is an integer or float v, return AudioFrame(self.array * v). Useful
        for controlling the total volume of frames.
    
        """
        if isinstance(other, AudioFrame):
            if self.length != other.length:
                raise TypeError("cannot stack frames of different lengths: "
                                "use slicing to resize the frames before mixing")
            return AudioFrame(np.vstack((self.array, other.array)),
                              **self.properties)
    
        elif isinstance(other, (int, float)):
            return AudioFrame(self.array * other, **self.properties)
    
        return NotImplemented

class Oscillator(ElkBaseClass):
    """Oscillator(waveform, freq=440.0, phase=0.0, **kwargs)"""
    def __init__(self, waveform: 'Waveform', *,
                 freq: Union[float, Iterator[float]] = 440.0,
                 phase: Union[float, Iterator[float]] = 0.0,
                 **kwargs) -> None:
        """Create an Oscillator."""
        ElkBaseClass.__init__(self, **kwargs)

        self.waveform = waveform
        self.set_freq(freq)
        self.set_phase(phase)
        self.update_state()
        self._dynamic_phase = 0.0

    def __repr__(self):
        return ("Oscillator(waveform={waveform}, freq(0)={freq}, phase(0)={phase}, "
                "bpm={bpm}, rate={rate})"
                .format(waveform=self.waveform, freq=self.freq,
                        phase=self.phase, **self.properties))
    def copy(self, **kwargs) -> 'Oscillator':
        """Return a copy of self with variables substituted by **kwargs."""
        return Oscillator(**{'waveform': self.waveform, 'freq': self._freq,
                             'phase': self._phase, **self.properties, **kwargs})
    __call__ = copy
    def __iter__(self) -> 'Oscillator':
        return self
    
    def __next__(self, f: float = 1.0, p: float = 0.0) -> float:
        self._dynamic_phase += (2 * np.pi * f * self.freq) / self.rate
        val = self.waveform(self._dynamic_phase + p + self.phase)
        self.update_state()
        return val
    
    def reset(self) -> None:
        self._dynamic_phase = 0
    def set_freq(self, freq: Union[float, int, Iterator]):
        from collections import abc
        from itertools import repeat
    
        self._freq = freq if isinstance(freq, abc.Iterator) else repeat(freq)
    
    def set_phase(self, phase: Union[float, int, Iterator]):
        from collections import abc
        from itertools import repeat
    
        self._phase = phase if isinstance(phase, abc.Iterator) else repeat(phase)
    
    def update_state(self):
        """Update the state by advancing one sample."""
        self.freq = next(self._freq)
        self.phase = next(self._phase)
    @property
    def modulators(self) -> Mapping[str, Iterator]:
        return {'freq': self._freq, 'phase': self._phase}
    def __getitem__(self, key: Union[int, float, slice]) -> AudioFrame:
        from itertools import islice
    
        if isinstance(key, slice):
            keystart = 0 if key.start is None else key.start
            keystop = (keystart + 1 - keystart % 1 if key.stop is None else
                       key.stop)
            start = keystart * self.rate * 60 // self.bpm
            stop = keystop * self.rate * 60 // self.bpm
    
        elif isinstance(key, (int, float)):
            start = key * self.rate * 60 // self.bpm
            stop = start
        else:
            raise TypeError("invalid key")
    
        return AudioFrame([*islice(self, int(start), int(stop))],
                          **self.properties)
    def __add__(self, other) -> Union['Oscillator', 'OscillatorCollection']:
        from operator import add
    
        if isinstance(other, (int, float)):
            return self(waveform=self.waveform + other)
        elif isinstance(other, (Oscillator, OscillatorCollection)):
            return OscillatorCollection(self, other, operator=add)
    
        return NotImplemented
    __radd__ = __add__
    
    def __mul__(self, other) -> Union['Oscillator', 'OscillatorCollection']:
        from operator import mul
    
        if isinstance(other, (int, float)):
            return self(waveform=self.waveform * other)
        elif isinstance(other, (Oscillator, OscillatorCollection)):
            return OscillatorCollection(self, other, operator=mul)
    
        return NotImplemented
    __rmul__ = __mul__

from operator import add, mul

class OscillatorCollection(Oscillator):
    """OscillatorCollection(*oscillators, operator=add, **kwargs)"""
    def __init__(self, *oscillators, operator=add, base_freq: float = 440.0,
                 base_phase: float = 0.0, **kwargs) -> None:
        """Create an OscillatorCollection."""
        Oscillator.__init__(self, waveform=const, **kwargs)

        self.base_freq = base_freq
        self.base_phase = base_phase

        self.oscillators = oscillators
        self.operator = operator

    @property
    def factors(self):
        """Return a tuple of frequency and phase factors.

        The factors are returned relative to their initial value.

        """
        return (self.freq / self.base_freq,
                self.phase - self.base_phase)

    def __repr__(self) -> str:
        return ("Osc{}({}, freq={}, phase={})".format(
            self.operator.__name__.capitalize(),
            ", ".join(repr(o) for o in self.oscillators),
            self.freq, self.phase))
    def copy(self, **kwargs) -> 'OscillatorCollection':
        """Return a copy of self with variables substituted by **kwargs."""
        return OscillatorCollection(
            *(oscillator.copy() for oscillator in self.oscillators),
            **{'operator': self.operator, **self.properties, **kwargs})
    __call__ = copy
    def __add__(self, other):
        if isinstance(other, (Oscillator, OscillatorCollection)):
            return OscillatorCollection(self, other, operator=add)
    
        return NotImplemented
    __radd__ = __add__
    
    def __mul__(self, other):
        if isinstance(other, (Oscillator, OscillatorCollection)):
            return OscillatorCollection(self, other, operator=mul)
    
        return NotImplemented
    __rmul__ = __mul__
    def __iter__(self) -> 'OscillatorCollection':
        return self
    
    def __next__(self, f: float = 1.0, p: float = 0.0) -> float:
        from functools import reduce
    
        factors = (f * self.factors[0], p + self.factors[1])
        val = self.operator(self.oscillators[0].__next__(*factors),
                            self.oscillators[1].__next__(*factors))
        self.update_state()
        return val
    
    def reset(self):
        for oscillator in self.oscillators:
            oscillator.reset()

def forward_linear_dy(a, b, dx):
    if a[0] >= b[0]:
        return b[1] - a[1]
    return dx * (b[1] - a[1]) / (b[0] - a[0])

class Envelope(ElkBaseClass):
    """Envelope(points, **kwargs)"""
    def __init__(self, points: list, values: list,
                 dy: Callable = forward_linear_dy, follow_freq: bool = False,
                 position: float = 0.0, **kwargs) -> None:
        """Create an Envelope."""
        ElkBaseClass.__init__(self, **kwargs)

        if len(points) != len(values):
            raise ValueError("points and values lists must have equal length")

        self.points = points
        self.values = values
        self.follow_freq = follow_freq
        self.dy = dy
        self._initial_position = position

        self.reset()

    def __repr__(self):
        return ("Envelope(points={points}, values={values}, position={position}, "
                "kind={kind}, follow_freq={follow_freq}, bpm={bpm}, rate={rate})"
                .format(points=self.points, values=self.values,
                        position=self.position, kind=self.kind,
                        follow_freq=self.follow_freq, **self.properties))
    def copy(self, position: float = 0.0, **kwargs) -> 'Envelope':
        """Return a copy of self with variables substituted by **kwargs."""
        return Envelope(**{'points': self.points, 'values': self.values,
                           'dy': self.dy, 'follow_freq': self.follow_freq,
                           'position': position, **self.properties, **kwargs})
    __call__ = copy
    def __iter__(self) -> 'Envelope':
        return self
    
    def update_state(self) -> None:
        while (self._target_index < len(self.points) - 1 and
               self.position >= self._target_point):
            self._target_index +=1
            self._target_point = self.points[self._target_index]
            self._target_value = self.values[self._target_index]
    
    def __next__(self, f: float = 1.0, p: float = 0.0) -> float:
        if not self.follow_freq:
            f = 1
    
        dx = f * self.bpm / 60 / self.rate
        dy = self.dy((self.position, self.value),
                     (self._target_point, self._target_value),
                     dx)
    
        self.position += dx
        self.value += dy
    
        self.update_state()
    
        return self.value
    
    def reset(self) -> None:
        """Reset the internal state."""
        self.position = self._initial_position
        self.value = self.values[0]
    
        self._target_index = 0
        self._target_point = self.points[0]
        self._target_value = self.values[0]
    
        self.update_state()
    
    def __mul__(self, other) -> Union['Oscillator', 'OscillatorCollection',
                                      'Envelope']:
        from operator import mul
    
        if isinstance(other, (Oscillator, OscillatorCollection, Envelope)):
            return OscillatorCollection(self, other, operator=mul)
    
        return NotImplemented
    __rmul__ = __mul__
    
    def __add__(self, other) -> Union['Oscillator', 'OscillatorCollection',
                                      'Envelope']:
        from operator import add
    
        if isinstance(other, (Oscillator, OscillatorCollection, Envelope)):
            return OscillatorCollection(self, other, operator=add)
    
        return NotImplemented
    __radd__ = __add__
    def __getitem__(self, key: Union[int, float, slice]) -> AudioFrame:
        from itertools import islice
    
        if isinstance(key, slice):
            keystart = 0 if key.start is None else key.start
            keystop = (keystart + 1 - keystart % 1 if key.stop is None else
                       key.stop)
            start = keystart * self.rate * 60 // self.bpm
            stop = keystop * self.rate * 60 // self.bpm
    
        elif isinstance(key, (int, float)):
            start = key * self.rate * 60 // self.bpm
            stop = start
        else:
            raise TypeError("invalid key")
    
        return AudioFrame([*islice(self, int(start), int(stop))],
                          **self.properties)

class Waveform:
    """Waveform(name, function, offset=0.0, scale=1.0)"""
    def __init__(self, name: str, function: Callable[[float], float], *,
                 offset: float = 0.0, scale: float = 1.0,
                 inverted: bool = False) -> None:
        """Create a Waveform."""
        self.__dict__.update(locals())

    def __repr__(self) -> str:
        return "{scale} * {i}{name}(t) + {offset}".format(**self.__dict__,
                                                          i="~" * self.inverted)
    def __call__(self, t: float, **kwargs) -> float:
        """Return self.function(t mod 2 pi)."""
        kwargs = {'scale': self.scale, 'offset': self.offset, **kwargs}
        scale, offset = kwargs['scale'], kwargs['offset']
        d = -1 if self.inverted else 1
        return scale * self.function(d * t % (2 * np.pi)) + offset
    def copy(self, **kwargs) -> 'Waveform':
        """Return a copy of self."""
        return Waveform(**{'name': self.name, 'function': self.function,
                           'offset': self.offset, 'scale': self.scale,
                           'inverted': self.inverted, **kwargs})
    def __add__(self, other) -> 'Waveform':
        if isinstance(other, (int, float)):
            return self.copy(offset=self.offset + other)
    
        return NotImplemented
    __radd__ = __add__
    
    def __mul__(self, other) -> 'Waveform':
        if isinstance(other, (int, float)):
            return self.copy(scale=self.scale * other)
    
        return NotImplemented
    __rmul__ = __mul__
    
    def __invert__(self) -> 'Waveform':
        return self.copy(inverted=not self.inverted)

sin = Waveform("sin", np.sin)
sqr = Waveform("sqr", lambda t: np.sign(np.sin(t)))
tri = Waveform("tri", lambda t: (2 * t / np.pi if t < np.pi / 2 else
                                 2 - 2 * t / np.pi if t < 3 * np.pi / 2 else
                                 2 * t / np.pi - 4))
saw = Waveform("saw", lambda t: (1 + t / np.pi) % 2 - 1)

sinz = Waveform("sinz", lambda t: (1 - np.cos(t)) / 2)
sqrz = Waveform("sqrz", lambda t: lambda t: (1 - np.sign(np.sin(t)) / 2))
triz = Waveform("triz", lambda t: abs((1 + t / np.pi) % 2 - 1))
sawz = Waveform("sawz", lambda t: t / (2 * np.pi))

noise = Waveform("noise", lambda t: np.random.random() * 2 - 1)
noisez = Waveform("noisez", lambda t: np.random.random())
const = Waveform("const", lambda t: 0)

osc = Oscillator(sin)
